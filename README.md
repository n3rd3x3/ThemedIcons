# ThemedIcons
A maintained version of a Magisk module that adds more themed icons to the Pixel launcher. 

Credit to [Team Files Module Repo](https://t.me/modulerepo) for the original module as well as the new icons.
