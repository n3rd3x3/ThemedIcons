# v1.1.1
Fix a part of the module so it can actually install

# v1.1.0
Update icons to latest icons from Pixel Launcher Mod

# v1.0.0
Initial release, unedited files from the origional module.
